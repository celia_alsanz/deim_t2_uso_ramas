using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision) {
        Destroy(gameObject);
        Debug.Log("Coleccionable recogido");
    }
}
